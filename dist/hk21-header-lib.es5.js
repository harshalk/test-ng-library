import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
var HeaderComponent = (function () {
    function HeaderComponent() {
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = function () {
    };
    return HeaderComponent;
}());
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-header',
                template: "\n    <h1>\n      <hr>\n      <p>Powered by HK21</p>\n\n      <ng-content></ng-content>\n      <hr>\n    </h1>\n  ",
                styles: ["\n    h1 {\n        color: wheat;\n        background-color: blueviolet\n      }\n\n    p {\n        font-size: 12px;\n    }  \n  "]
            },] },
];
/**
 * @nocollapse
 */
HeaderComponent.ctorParameters = function () { return []; };
var HeaderModule = (function () {
    function HeaderModule() {
    }
    return HeaderModule;
}());
HeaderModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [HeaderComponent],
                exports: [
                    HeaderComponent
                ]
            },] },
];
/**
 * @nocollapse
 */
HeaderModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */
export { HeaderModule, HeaderComponent as ɵa };
//# sourceMappingURL=hk21-header-lib.es5.js.map
